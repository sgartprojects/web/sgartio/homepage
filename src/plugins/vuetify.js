import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
    icons: {
        iconfont: 'mdi',
    },
    theme: {
        dark: !window.matchMedia('(prefers-color-scheme: light)').matches,
        options: {
            cspNonce: "u34hru43ht54tjh536u5h6k54h6j54h6k54h6k45"
        }
    },
});
